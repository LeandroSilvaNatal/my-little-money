﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Item : MonoBehaviour {

	public int dia;
	public List<Preset> presets;
	public Owner owner;
	
	// Use this for initialization
	void Start () {
		build();
		dia = 0;
		InvokeRepeating("aplicarPreset",1f,1f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void aplicarPreset(){
		dia = dia + 1;
		for(Preset presets : presets){
			owner.aplicarPreset(preset, dia);
		}
	}

	public abstract void build(); //Cria a lista de presets. Cria a lista de Movimentações
}
